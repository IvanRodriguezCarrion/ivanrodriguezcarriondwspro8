<?php

function recoge($valor) {
    if (isset($_REQUEST[$valor])) {
	$campo = htmlspecialchars(trim(strip_tags($_REQUEST[$valor])));
    } else {
	$campo = "";
    };
    return $campo;
}

function comprobarModelo() {

    if ($_SESSION['modelo'] === "mysql") {
	$modeloElegido = new ModeloMysql();
    } else if ($_SESSION['modelo'] === "ficheros") {
	$modeloElegido = new ModeloFicheros();
    }
    return $modeloElegido;
}

function acceso() {
    if ($_SESSION['autenticar'] !== "autenticado") {
	header("Location: ../index.php");
    }
}

function botonDesloguear() {
    echo " <div align='left'><form action='../controlador/ControladorLogOut.php' method='post' >
	    <input type='submit' name='desloguear' value ='Cerrar sesión'/>
	</form></div>";
   
}


?>