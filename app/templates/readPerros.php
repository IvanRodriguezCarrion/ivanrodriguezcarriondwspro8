<?php ob_start(); ?>

<form action="index.php?ctl=buscar-perro" method="POST">
    <p>
	Buscar por ID: <input type="text" name="buscar" placeholder="Introduzca ID">
	<button type="submit">Buscar</button>
    </p>
</form>

<table class="table table-striped">
    <thead>
    <th>ID</th>
    <th>Nombre</th>
    <th>Raza</th>
    <th>Número Chip</th>
    <th>Propietario</th>
    <th>Acción</th>
</thead>
<tbody>
    <?php foreach ($params['perros'] as $perro) : ?>

        <tr>
    	<td><?php echo $perro->__GET('id') ?></td>
    	<td><?php echo $perro->__GET('nombre') ?></td>
    	<td><?php echo $perro->__GET('raza') ?></td>
	<td><?php echo $perro->__GET('numChip') ?></td>
	<td><?php echo $perro->__GET('propietario')->__GET('id') ?></td>
    	<td>
		<?php if ($perro->__GET('id') != "") : ?>
		    <a class='btn btn-warning' href="index.php?ctl=editar-perro&id=<?php echo $perro->__GET('id') ?>">Editar</a>
		    <a class='btn btn-danger' href="index.php?ctl=borrar-perro&id=<?php echo $perro->__GET('id') ?>">Eliminar</a>
		<?php endif; ?>
    	</td>
        </tr>
    <?php endforeach; ?>    
</tbody>
</table>

<?php
$contenido = ob_get_clean();
$titulo = "Perros";
include "layout.php";
?>