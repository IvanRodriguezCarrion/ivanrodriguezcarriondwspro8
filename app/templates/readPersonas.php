<?php ob_start(); ?>
<form action="index.php?ctl=buscar-persona" method="POST">
    <p>
	Buscar por ID: <input type="text" name="buscar" placeholder="Introduzca ID">
	<button type="submit">Buscar</button>
    </p>
</form>

<table class="table table-striped">
    <thead>
    <th>ID</th>
    <th>Nombre</th>
    <th>Apellido</th>
    <th>Acción</th>
</thead>
<tbody>
    <?php foreach ($params['personas'] as $persona) : ?>

        <tr>
    	<td><?php echo $persona->__GET('id') ?></td>
    	<td><?php echo $persona->__GET('nombre') ?></td>
    	<td><?php echo $persona->__GET('apellidos') ?></td>
    	<td>
		<?php if ($persona->__GET('id') != "") : ?>
	    <button class='boton'><a href="index.php?ctl=editar-persona&id=<?php echo $persona->__GET('id') ?>">Editar</a></button>
		<?php endif; ?>
		<?php if ($persona->__GET('id') != "") : ?>
		    <button class='boton'><a href="index.php?ctl=borrar-persona&id=<?php echo $persona->__GET('id') ?>">Eliminar</a></button>
		<?php endif; ?>
    	</td>
        </tr>
    <?php endforeach; ?>    
</tbody>
</table>

<?php
$contenido = ob_get_clean();
$titulo = "Personas";
include "layout.php";
?>