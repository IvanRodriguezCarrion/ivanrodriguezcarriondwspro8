<?php ob_start(); ?>

<h3> Editar perro</h3>

<form action="index.php?ctl=actualizar-perro" method="post" >
    <table >
	<tr>
	    <th >Identificador</th>
	    <td><input type="text" name="id" value="<?php echo $params['perro']->__GET('id'); ?>" readonly="readonly" /></td>
	</tr>
	<tr>
	    <th >Nombre</th>
	    <td><input type="text" name="nombre" value="<?php echo $params['perro']->__GET('nombre'); ?>" placeholder="nombre del perro" /></td>
	</tr>
	<tr>
	    <th >Raza</th>
	    <td><input type="text" name="raza" value="<?php echo $params['perro']->__GET('raza'); ?>"  /></td>
	</tr>
	<tr>
	    <th >Número Chip</th>
	    <td><input type="text" name="numChip" value="<?php echo $params['perro']->__GET('numChip'); ?>"  /></td>
	</tr>
	<tr>
	    <th >Proprietario</th>
	    <td>
		<select name="propietario">
		    <option value ="0"> Escoja un propietario </option>
		    <?php foreach ($params['personas'] as $personas) : ?> 
		    <?php if ($params['perro']->__GET('propietario')->__GET('id') == $personas->__GET('id')): ?>
			<option value="<?php echo $personas->__GET('id') ?>" selected>
			<?php else: ?>
			    <option value="<?php echo $personas->__GET('id') ?>">
			<?php endif; ?>
			    <?php echo $personas->__GET('nombre')?>
			</option>
		    <?php endforeach; ?>  
		</select>
	    </td>
	</tr>
	<tr>
	    <td colspan="2">
		<button type="submit">Guardar</button>
	    </td>
	</tr>
    </table>
</form>

<?php
$contenido = ob_get_clean();
$titulo = 'Editar perro';
include 'layout.php';
?>