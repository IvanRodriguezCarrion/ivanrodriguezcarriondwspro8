<?php ob_start(); ?>

<h3> Alta de nuevos perros</h3>

<form action="index.php?ctl=nuevo-perro" method="post" >
    <table >
	<tr>
	    <th >Identificador</th>
	    <td><input type="text" name="id" value="<?php echo $params['id']; ?>" readonly="readonly" /></td>
	</tr>
	<tr>
	    <th >Nombre</th>
	    <td><input type="text" name="nombre" value="" placeholder="nombre del perro" /></td>
	</tr>
	<tr>
	    <th >Raza</th>
	    <td><input type="text" name="raza" value=""  /></td>
	</tr>
	<tr>
	    <th >Número Chip</th>
	    <td><input type="text" name="numChip" value=""  /></td>
	</tr>
	<tr>
	    <th >Proprietario</th>
	    <td>
		<select name="propietario">
		    <option value ="0"> Escoja un propietario </option>
		    <?php foreach ($params['personas'] as $personas) : ?>
    		    <option value="<?php echo $personas->__GET('id') ?>">
			    <?php echo $personas->__GET('nombre')?>
    		    </option>
		    <?php endforeach; ?>  
		</select>
	    </td>
	</tr>
	<tr>
	    <td colspan="2">
		<button type="submit">Guardar</button>
	    </td>
	</tr>
    </table>
</form>

<?php
$contenido = ob_get_clean();
$titulo = 'Crear nuevo perro';
include 'layout.php';
?>