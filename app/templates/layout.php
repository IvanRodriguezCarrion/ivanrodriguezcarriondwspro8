<!DOCTYPE html>
<html lang="es">
    <head>
        <title><?php echo $titulo ?> | Aplicación de apadrinamiento</title>
        <meta charset="UTF-8">
	<meta charset="UTF-8">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!--	<style>
	    body {
		margin: 10px;
		padding: 10px;
	    }
	</style>
	<style>
	    .boton a {
		text-decoration: none;
	    }
	    .boton a:link {
		color:black;
	    }
	    .boton a:visited {
		color:black;
	    }
	</style>-->
    </head>
    <body>
	<nav>
	    <h1>Navegación por web</h1>
	    <ul class="nav nav-tabs nav-justified">
		<li><a href="index.php?ctl=inicio">Regresar al inicio</a></li>
		<li><a href="index.php?ctl=instalar">Instalar base de datos</a></li>
		<li><a href="index.php?ctl=desinstalar">Desinstalar base de datos</a></li>
		<li><a href="index.php?ctl=seleccionar-base-datos">Escoger base de datos</a></li>
		<li><a href="index.php?ctl=salir">Salir de la aplicación</a></li>
		<li><a href="documentacion/documentacion.pdf">Documentación</a></li>
		
		
	    </ul>
	    <ul class="nav nav-pills nav-justified">
		<li><a href="index.php?ctl=crear-persona">Nueva persona</a></li>
		<li><a href="index.php?ctl=personas">Leer personas</a></li>
		<li><a href="index.php?ctl=crear-perro">Nuevo perro</a></li>
		<li><a href="index.php?ctl=perros">Leer perros</a></li>
	    </ul>
	</nav>
	<?php if (isset($params['mensaje'])) : ?>
    	<p style="color:red;">
		<?php echo $params['mensaje']['texto'] ?>
    	</p>
	<?php endif; ?>
	<div class="container-fluid">
	    <?php echo $contenido ?>
	</div>


	<footer>
	    <?php
	    echo "<hr/><pre>" . Configuracion::$empresa . " " . Configuracion::$autor . " ";
	    echo Configuracion::$curso . " " . Configuracion::$fecha . "</pre>\n";
	    ?>
	</footer>

    </body>
</html>