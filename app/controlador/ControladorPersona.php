<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ControladorPersona
 *
 * @author BlackBana
 */
class ControladorPersona {

    function read() {
	if (comprobarModelo() != null) {
	    $modelo = comprobarModelo();
	    $params = array('personas' => $modelo->readPersona());
	    require __DIR__ . '/../templates/readPersonas.php';
	} else {
	    require __DIR__ . '/../templates/error.php';
	}
    }

    public function create() {
	if (comprobarModelo() != null) {
	    $modelo = comprobarModelo();
	    $params = array('id' => $modelo->idPersona());
	    require __DIR__ . '/../templates/crearPersona.php';
	} else {
	    require __DIR__ . '/../templates/error.php';
	}
    }

    function save() {
	$id = recoge('id');
	$nombre = recoge('nombre');
	$apellido = recoge('apellido');
	if (comprobarModelo() != null) {
	    $modelo = comprobarModelo();
	    if ($id == "" || $nombre == "" || $apellido == "") {
		$params = array('id' => $modelo->idPersona());
		$params['mensaje'] = array('texto' => 'Alguno de los campos está vacío.');
		require __DIR__ . '/../templates/crearPersona.php';
	    } else {
		$persona = new Persona($id, $nombre, $apellido);
		$modelo->createPersona($persona);
		$params = array('personas' => $modelo->readPersona());
		$params['mensaje'] = array('texto' => 'Persona de nombre ' . $nombre . ' creada con éxito.');
		require __DIR__ . '/../templates/readPersonas.php';
	    }
	} else {
	    require __DIR__ . '/../templates/error.php';
	}
    }

    function update() {
	$id = $_GET['id'];
	if (comprobarModelo() != null) {
	    $modelo = comprobarModelo();
	    $params['persona'] = $modelo->searchPersona($id);
	    require __DIR__ . '/../templates/editarPersona.php';
	} else {
	    require __DIR__ . '/../templates/error.php';
	}
    }

    function edit() {
	$id = recoge('id');
	$nombre = recoge('nombre');
	$apellido = recoge('apellido');
	if (comprobarModelo() != null) {
	    $modelo = comprobarModelo();
	    if ($id == "" || $nombre == "" || $apellido == "") {
		$params['persona'] = new Persona($id, $nombre, $apellido);
		$params['mensaje'] = array('texto' => 'No puede haber campos vacíos. Rellénalos.');
		require __DIR__ . '/../templates/editarPersona.php';
	    } else {
		$persona = new Persona($id, $nombre, $apellido);
		$modelo->updatePersona($persona);
		$params = array('personas' => $modelo->readPersona());
		$params['mensaje'] = array('texto' => 'Persona de nombre ' . $nombre . ' actualizada con éxito.');
		require __DIR__ . '/../templates/readPersonas.php';
	    }
	} else {
	    require __DIR__ . '/../templates/error.php';
	}
    }

    function delete() {
	$id = recoge('id');
	if (comprobarModelo() != null) {
	    $modelo = comprobarModelo();
	    $persona = $modelo->searchPersona($id);
	    $modelo->deletePersona($persona);
	    $params = array('personas' => $modelo->readPersona());
	    $params['mensaje'] = array('texto' => 'Persona con nombre ' . $persona->__GET('nombre') . ' eliminada con éxito.');
	    require __DIR__ . '/../templates/readPersonas.php';
	} else {
	    require __DIR__ . '/../templates/error.php';
	}
    }

    function search() {
	$id = recoge('buscar');
	if (comprobarModelo() != null) {
	    $modelo = comprobarModelo();
	    $persona = $modelo->searchPersona($id);
		if ($persona == null) {
		    $params['personas'] = array(new Persona("", "", ""));
		    $params['mensaje'] = array('texto' => 'No se encuentran coincidencias con el id introducido.');
		} else {
		    $params['personas'] = array($modelo->searchPersona($id));
		}
		require __DIR__ . '/../templates/readPersonas.php';
	} else {
	    require __DIR__ . '/../templates/error.php';
	}
    }

}
