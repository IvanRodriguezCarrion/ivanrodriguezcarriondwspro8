<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ControladorPerro
 *
 * @author BlackBana
 */
class ControladorPerro {

    function read() {
	if (comprobarModelo() != null) {
	    $modelo = comprobarModelo();
	    $params = array('perros' => $modelo->readPerro());
	    require __DIR__ . '/../templates/readPerros.php';
	} else {
	    require __DIR__ . '/../templates/error.php';
	}
    }

    public function create() {
	if (comprobarModelo() != null) {
	    $modelo = comprobarModelo();
	    $params = array('id' => $modelo->idPerro(), 'personas' => $modelo->readPersona());
	    require __DIR__ . '/../templates/crearPerro.php';
	} else {
	    require __DIR__ . '/../templates/error.php';
	}
    }

    function save() {
	$id = recoge('id');
	$nombre = recoge('nombre');
	$raza = recoge('raza');
	$numChip = recoge('numChip');
	$propietario = recoge('propietario');
	if (comprobarModelo() != null) {
	    $modelo = comprobarModelo();
	    if ($id == "" || $nombre == "" || $raza == "" || $numChip=="" || $propietario=="") {
		$params = array('id' => $modelo->idPerro(), 'personas' => $modelo->readPersona());
		$params['mensaje'] = array('texto' => 'Alguno de los campos está vacío.');
		require __DIR__ . '/../templates/crearPerro.php';
	    } else {
		$persona = new Persona($propietario, null, null);
		$perro = new Perro($id, $nombre, $raza, $numChip, $persona);
		$modelo->createPerro($perro);
		$params = array('perros' => $modelo->readPerro());
		$params['mensaje'] = array('texto' => 'Perro de nombre ' . $nombre . ' creada con éxito.');
		require __DIR__ . '/../templates/readPerros.php';
	    }
	} else {
	    require __DIR__ . '/../templates/error.php';
	}
    }

    function update() {
	$id = $_GET['id'];
	if (comprobarModelo() != null) {
	    $modelo = comprobarModelo();
	    $params['perro'] = $modelo->searchPerro($id);
	    $params['personas'] = $modelo->readPersona();
	    require __DIR__ . '/../templates/editarPerro.php';
	} else {
	    require __DIR__ . '/../templates/error.php';
	}
    }

    function edit() {
	$id = recoge('id');
	$nombre = recoge('nombre');
	$raza = recoge('raza');
	$numChip = recoge('numChip');
	$propietario = recoge('propietario');
	if (comprobarModelo() != null) {
	    $modelo = comprobarModelo();
	    if ($id == "" || $nombre == "" || $raza == "" || $numChip=="" || $propietario=="0") {
		$params = array('id' => $modelo->idPerro(), 'personas' => $modelo->readPersona());
		$params['mensaje'] = array('texto' => 'Alguno de los campos está vacío.');
		require __DIR__ . '/../templates/readPerros.php';
	    } else {
		$persona = new Persona($propietario, null, null);
		$perro = new Perro($id, $nombre, $raza, $numChip, $persona);
		$modelo->updatePerro($perro);
		$params = array('perros' => $modelo->readPerro());
		$params['mensaje'] = array('texto' => 'Perro de nombre ' . $nombre . ' actualizado con éxito.');
		require __DIR__ . '/../templates/readPerros.php';
	    }
	} else {
	    require __DIR__ . '/../templates/error.php';
	}
    }

    function delete() {
	$id = recoge('id');
	if (comprobarModelo() != null) {
	    $modelo = comprobarModelo();
	    $perro = $modelo->searchPerro($id);
	    $modelo->deletePerro($perro);
	    $params = array('perros' => $modelo->readPerro());
	    $params['mensaje'] = array('texto' => 'Perro con nombre ' . $perro->__GET('nombre') . ' eliminado con éxito.');
	    require __DIR__ . '/../templates/readPerros.php';
	} else {
	    require __DIR__ . '/../templates/error.php';
	}
    }

    function search() {
	$id = recoge('buscar');
	if (comprobarModelo() != null) {
	    $modelo = comprobarModelo();
	    $perro = $modelo->searchPerro($id);
		if ($perro == null) {
		    $persona = new Persona("","","");
		    $params['perros'] = array(new Perro("", "", "","",$persona));
		    $params['mensaje'] = array('texto' => 'No se encuentran coincidencias con el id introducido.');
		} else {
		    $params['perros'] = array($modelo->searchPerro($id));
		}
		require __DIR__ . '/../templates/readPerros.php';
	} else {
	    require __DIR__ . '/../templates/error.php';
	}
    }

}
