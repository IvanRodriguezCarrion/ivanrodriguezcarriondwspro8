<?php

class ControladorPrincipal {
    function start() {
        require __DIR__ . '/../templates/inicio.php';
    }
    
    function error() {
        require __DIR__ . '/../templates/error.php';
    }
            
    function instalar() {
        $modelo = comprobarModelo();
        $modelo->instalarBD();
        $params['mensaje'] = array('tipo' => 'success', 'texto' => 'Base de datos ' . $_SESSION['modelo'] . ' instalada con datos de ejemplo.');
        
        require __DIR__ . '/../templates/inicio.php';
    }
    
        function desinstalar() {
        $modelo = comprobarModelo();
        $modelo->desinstalarBD();
        $params['mensaje'] = array('texto' => 'Base de datos ' . $_SESSION['modelo'] . ' desinstalada con éxito.');
        
        require __DIR__ . '/../templates/inicio.php';
    }
    
    function seleccionarBD() {
        if (isset($_SESSION['modelo'])) {
            $params = array('modelo' => $_SESSION['modelo']);
        }
       
        require __DIR__ . '/../templates/configurarBD.php';
    }

    function configurarBD() {
        $bd = recoge('modelo');
        $_SESSION['modelo'] = $bd;
        $params = array('modelo' => $_SESSION['modelo']);
        $params['mensaje'] = array('tipo' => 'success', 'texto' => 'Escogido ' . $bd . ' como sistema de almacenamiento.');
        require __DIR__ . '/../templates/inicio.php';
    }

    function login() {
        $usuario = recoge("nombre");
        $password = recoge("pass");
        if ($usuario === "admin" && $password === "admin") {
            $_SESSION["autenticar"] = "autenticado"; 
            if (!isset($_SESSION['modelo'])) {
                require __DIR__.'/../templates/configurarBD.php';
            } else {
                require __DIR__.'/../templates/inicio.php';
            }
        } else {
           require __DIR__.'/../templates/login.php';
        }
    }

    function logout() {
        unset($_SESSION["autenticar"]);
        //unset($_SESSION["modelo"]);
        session_destroy();

        require __DIR__ . '/../templates/login.php';
    }
    
}
