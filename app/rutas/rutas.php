<?php

$rutas = array (
    //Rutas generales
    'inicio' => array('controller'=>'ControladorPrincipal', 'action'=>'start'),
    'login' => array('controller'=>'ControladorPrincipal', 'action'=>'login'),
    'error' => array('controller'=>'ControladorPrincipal', 'action'=>'error'),
    'salir' => array('controller'=>'ControladorPrincipal', 'action'=>'logout'),
    
    //Rutas de bases de datos
    'instalar'=> array('controller'=>'ControladorPrincipal', 'action'=>'instalar'),
    'desinstalar'=> array('controller'=>'ControladorPrincipal', 'action'=>'desinstalar'),
    'configurar'=> array('controller'=>'ControladorPrincipal', 'action'=>'configurarBD'),
    'seleccionar-base-datos'=> array('controller'=>'ControladorPrincipal', 'action'=>'seleccionarBD'),
    
    //Rutas para Persona
    'personas'=> array('controller'=>'ControladorPersona', 'action'=>'read'),
    'crear-persona'=> array('controller'=>'ControladorPersona', 'action'=>'create'),
    'nueva-persona'=> array('controller'=>'ControladorPersona', 'action'=>'save'),
    'editar-persona'=> array('controller'=>'ControladorPersona', 'action'=>'update'),
    'actualizar-persona'=> array('controller'=>'ControladorPersona', 'action'=>'edit'),
    'buscar-persona'=> array('controller'=>'ControladorPersona', 'action'=>'search'),
    'borrar-persona'=> array('controller'=>'ControladorPersona', 'action'=>'delete'),
    
    //Rutas para Perro
    'perros'=> array('controller'=>'ControladorPerro', 'action'=>'read'),
    'crear-perro'=> array('controller'=>'ControladorPerro', 'action'=>'create'),
    'nuevo-perro'=> array('controller'=>'ControladorPerro', 'action'=>'save'),
    'editar-perro'=> array('controller'=>'ControladorPerro', 'action'=>'update'),
    'actualizar-perro'=> array('controller'=>'ControladorPerro', 'action'=>'edit'),
    'buscar-perro'=> array('controller'=>'ControladorPerro', 'action'=>'search'),
    'borrar-perro'=> array('controller'=>'ControladorPerro', 'action'=>'delete'),
    
	);

