<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author BlackBana
 */
interface Modelo {

    //metodos base de datos
    public function instalarBD();

    public function desinstalarBD();

    //metodos persona
    public function createPersona($persona);

    public function readPersona();

    public function updatePersona($persona);

    public function deletePersona($persona);

    public function idPersona();

    public function searchPersona($id);

    //metodos perro
    public function createPerro($perro);

    public function readPerro();

    public function updatePerro($perro);

    public function deletePerro($perro);

    public function idPerro();

    public function searchPerro($id);
}
