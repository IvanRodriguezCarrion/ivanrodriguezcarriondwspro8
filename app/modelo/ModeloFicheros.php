<?php

include_once( __DIR__.'/Persona.php');
include_once( __DIR__.'/Perro.php');
include_once( __DIR__.'/Modelo.php');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ModeloFicheros
 *
 * @author BlackBana
 */
class ModeloFicheros implements Modelo {

    private $fpersonas = "../app/database/personas.csv";
    private $fperros = "../app/database/perros.csv";
    
    public function instalarBD() {
	if (!file_exists($this->fpersonas)) {
	    $archivo = fopen($this->fpersonas, "w");
	    fclose($archivo);
	    $this->createPersona(new Persona(1, "Pepe", "Acebes"));
            $this->createPersona(new Persona(2, "Paco", "Roca"));
            $this->createPersona(new Persona(3, "Victor", "Dominguez"));
            $this->createPersona(new Persona(4, "Abedul", "Raices"));
        
	}
	if (!file_exists($this->fperros)) {
	    $archivo = fopen($this->fperros, "w");
	    fclose($archivo);
	    $this->createPerro(new Perro(1, "Perroncio", "Chucho",33532,"1"));
            $this->createPerro(new Perro(2, "Chuchangano", "Bicho",52124,"2"));
	}
    }
    

    public function createPersona($persona) {
	$f = fopen($this->fpersonas, "a");
	$linea = $persona->__GET('id') . ";"
		. $persona->__GET('nombre') . ";"
		. $persona->__GET('apellidos') . "\r\n";
	fwrite($f, $linea);
	fclose($f);
    }

    public function readPersona() {
	$personas = array();

	if ($archivo = fopen($this->fpersonas, "r")) {
	    $token = fgetcsv($archivo, 0, ";"); // fgetcsv se comporta como fgets pero para ficheros csv
	    while ($token) {
		$persona = new Persona($token[0], $token[1], $token[2]);
		array_push($personas, $persona);
		$token = fgetcsv($archivo, 0, ";");
	    }
	    fclose($archivo);
	} else {
	    echo "Esto es un error al abrir";
	}

	return $personas;
    }
    
    public function updatePersona($persona) {
	$id = $persona->__GET('id');
        if ($archivo = fopen($this->fpersonas, "r+")) {
            $f = fopen("../app/database/personas-temp.csv", "w+");
            $token = fgetcsv($archivo, 0, ";");
            while ($token) {
                if ($id != $token[0]) {
                    fputcsv($f, $token, ";");
                } else {
                    $linea = array($persona->__GET('id'), $persona->__GET('nombre'), $persona->__GET('apellidos'));
                    fputcsv($f, $linea, ";");
                }
                $token = fgetcsv($archivo, 0, ";");
            }
            fclose($archivo);
            fclose($f);
        }
        unlink("$this->fpersonas");
        rename("../app/database/personas-temp.csv", $this->fpersonas);
    }
    
     public function deletePersona($persona) {
	$id = $persona->__GET('id');
        if ($archivo = fopen($this->fpersonas, "r+")) {
            $f = fopen("../app/database/personas-temp.csv", "w+");
            $token = fgetcsv($archivo, 0, ";");
            while ($token) {
                if ($id != $token[0]) {
                    fputcsv($f, $token, ";");
                }
                $token = fgetcsv($archivo, 0, ";");
            }
            fclose($archivo);
            fclose($f);
        }
        unlink($this->fpersonas);
        rename("../app/database/personas-temp.csv", $this->fpersonas);
    }


    public function createPerro($perro) {
	$f = fopen($this->fperros, "a");
	$linea = $perro->__GET('id') . ";"
		. $perro->__GET('nombre') . ";"
		. $perro->__GET('raza') . ";"
		. $perro->__GET('numChip') . ";"
		. $perro->__GET('propietario') . "\r\n";
	fwrite($f, $linea);
	fclose($f);
    }

    public function readPerro() {
	$perros = array();

	if ($archivo = fopen($this->fperros, "r")) {
	    $token = fgetcsv($archivo, 0, ";"); // fgetcsv se comporta como fgets pero para ficheros csv
	    while ($token) {
		$persona = new Persona($token[4],null, null);
		$perro = new Perro($token[0], $token[1], $token[2], $token[3], $persona);
		array_push($perros, $perro);
		//agregar funcion recuperar nombre duenyo
		$token = fgetcsv($archivo, 0, ";");
	    }
	    fclose($archivo);
	} else {
	    //errorres
	}

	return $perros;
    }

    public function idPersona() {
	$modelo = new ModeloFicheros();
	$personas = $modelo->readPersona();
	$ultPersona = end($personas);
	$ultID = $ultPersona->__GET('id');
	$ultID = trim($ultID);
	$ultID++;
	return $ultID;
    }

    public function idPerro() {
	$modelo = new ModeloFicheros();
	$perros = $modelo->readPerro();
	$ultPerro = end($perros);
	$ultID = $ultPerro->__GET('id');
	$ultID = trim($ultID);
	$ultID++;
	return $ultID;
    }

    public function desinstalarBD() {
	unlink($this->fpersonas);
	unlink($this->fperros);
    }

    public function searchPerro($id) {
	$perro = null;
        if ($archivo = fopen($this->fperros, "r")) {
            $token = fgetcsv($archivo, 0, ";");
            while ($token) {
                if ($id == $token[0]) {
                    $persona = new Persona($token[4], null, null);
                    $perro = new Perro($token[0], $token[1], $token[2], $token[3], $persona);
                }
                $token = fgetcsv($archivo, 0, ";");
            }
            fclose($archivo);
        }
        return $perro;
    }

    public function searchPersona($id) {
	$persona = null;
        if ($archivo = fopen($this->fpersonas, "r")) {
            $token = fgetcsv($archivo, 0, ";");
            while ($token) {
                if ($id == $token[0]) {
                    $persona = new Persona($token[0], $token[1], $token[2]);
                }
                $token = fgetcsv($archivo, 0, ";");
            }
            fclose($archivo);
        }
        return $persona;        
    }

    public function deletePerro($perro) {
	$id = $perro->__GET('id');
        if ($archivo = fopen($this->fperros, "r+")) {
            $f = fopen("../app/database/perros-temp.csv", "w+");
            $token = fgetcsv($archivo, 0, ";");
            while ($token) {
                if ($id != $token[0]) {
                    fputcsv($f, $token, ";");
                }
                $token = fgetcsv($archivo, 0, ";");
            }
            fclose($archivo);
            fclose($f);
        }
        unlink($this->fperros);
        rename("../app/database/perros-temp.csv", $this->fperros);
    }

   

    public function updatePerro($perro) {
	$id = $perro->__GET('id');
        if ($archivo = fopen($this->fperros, "r+")) {
            $f = fopen("../app/database/perros-temp.csv", "w+");
            $token = fgetcsv($archivo, 0, ";");
            while ($token) {
                if ($id != $token[0]) {
                    fputcsv($f, $token, ";");
                } else {
                    $linea = array($perro->__GET('id'), $perro->__GET('nombre'), $perro->__GET('raza'), $perro->__GET('numChip'), $perro->__GET('propietario')->__GET('id'));
                    fputcsv($f, $linea, ";");
                }
                $token = fgetcsv($archivo, 0, ";");
            }
            fclose($archivo);
            fclose($f);
        }
        unlink($this->fperros);
        rename("../app/database/perros-temp.csv", $this->fperros);
    }
   

    
}
