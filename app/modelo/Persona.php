<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Persona {

    private $id;
    private $nombre;
    private $apellidos;
  
    public function __CONSTRUCT($id, $nombre, $apellidos) {
	$this->id = $id;
	$this->nombre = $nombre;
	$this->apellidos = $apellidos;
    }

    public function __GET($k) {
	return $this->$k;
    }

    public function __SET($k, $v) {
	return $this->$k = $v;
    }

}

?>