<?php
session_start();

require_once __DIR__ . '/../app/controlador/ControladorPrincipal.php';
require_once __DIR__ . '/../app/controlador/ControladorPerro.php';
require_once __DIR__ . '/../app/controlador/ControladorPersona.php';
require_once __DIR__ . '/../app/controlador/ControladorPrincipal.php';
require_once __DIR__ . '/../app/modelo/Persona.php';
require_once __DIR__ . '/../app/modelo/Perro.php';
require_once __DIR__ . '/../app/modelo/ModeloMysql.php';
require_once __DIR__ . '/../app/modelo/ModeloFicheros.php';
require_once __DIR__ . '/../app/Funciones.php';
require_once __DIR__ . '/../app/rutas/rutas.php';
require_once __DIR__ . '/../app/controlador/Configuracion.php';


if (isset($_SESSION["autenticar"]) && $_SESSION["autenticar"] == "autenticado") {
    if (isset($_GET['ctl'])) {
        if (isset($rutas[$_GET['ctl']])) {
            $ruta = $_GET['ctl'];
        } else {
            $ruta = 'error';
        }
    } else {
        $ruta = 'inicio';
    }
} else {
    $ruta = 'login';
}

$controlador = $rutas[$ruta];

if (method_exists($controlador['controller'], $controlador['action'])) {
    call_user_func(array(new $controlador['controller'], $controlador['action']));
} else {
    call_user_func(array('ControladorPrincipal', 'error'));
}